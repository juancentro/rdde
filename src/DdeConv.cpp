#include <Rcpp.h>

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN // Exclude rarely-used stuff from Windows headers
#endif

// Windows Header Files:
#include <windows.h>
#include <tchar.h>
#include <ddeml.h>
#include <string>
#include "DdeCallback.h"

class DdeConv {
public:

  std::string _server;
  std::string _topic;

  DdeConv(std::string server, std::string topic) {
    _server = server;
    _topic = topic;
    DWORD idInst = 0;
    UINT iReturn;
    iReturn = DdeInitialize(&idInst, (PFNCALLBACK)DdeCallback,
                            APPCLASS_STANDARD | APPCMD_CLIENTONLY, 0);
    if (iReturn == 0) {} else {}
    //DDE Connect to Server using given AppName and topic.
    HSZ hszApp, hszTopic;
    hszApp = DdeCreateStringHandle(idInst, server.data(), 0);
    hszTopic = DdeCreateStringHandle(idInst, topic.data(), 0);
    _hConv = NULL;
    _hConv = DdeConnect(idInst, hszApp, hszTopic, NULL);
    DdeFreeStringHandle(idInst, hszApp);
    DdeFreeStringHandle(idInst, hszTopic);
    _idInst = idInst;
  }

  std::string requestItem(std::string item)
  {
    std::string e;
    if (_hConv == NULL) return e;
    HSZ hszItem = DdeCreateStringHandle(_idInst, item.data(), 0);
    HDDEDATA hData = DdeClientTransaction(NULL, 0, _hConv, hszItem, CF_TEXT, XTYP_REQUEST, 5000, NULL);
    if (hData != 0) {
      TCHAR szResult[255];
      DdeGetData(hData, (unsigned char *)szResult, 255, 0);
      return std::string(szResult);
    } else {
      return e;
    }
  }

  bool isConvOk() {
    return _hConv != NULL;
  }

  ~DdeConv() {
    //DDE Disconnect and Uninitialize.
    DdeDisconnect(_hConv);
    DdeUninitialize(_idInst);
  }

//private:
  DWORD _idInst;
  HCONV _hConv;
};

void finalizeDdeConv (DdeConv *c) {
  //DDE Disconnect and Uninitialize.
  DdeDisconnect(c->_hConv);
  DdeUninitialize(c->_idInst);
}

RCPP_MODULE(mod_dde) {
  using namespace Rcpp;
  class_<DdeConv>("DdeConv")
  .constructor<std::string, std::string>()
  .field_readonly("server", &DdeConv::_server)
  .field_readonly("topic", &DdeConv::_topic)
  .method("requestItem", &DdeConv::requestItem)
  .method("isConvOk", &DdeConv::isConvOk)
  .finalizer(&finalizeDdeConv)
  ;
}

/*** R
d <- new(DdeConv, "EXCEL", "[DdeTest.xlsx]Sheet1")
d$requestItem("R1C1")
*/
